serve:
	@hugo server -Dwv -d public
	#@hugo server -Dwv -d public --bind="10.0.2.15"

compile:
	@hugo

deploy:
	@rsync --delete -avz public/ autodefesa:/www/

publish: compile deploy
