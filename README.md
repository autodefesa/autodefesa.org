# Autodefesa Digital

## Contribuindo

Use um pseudônimo se quiser:

    git config user.name "Nome Fantasia"
    git config user.email "email@fantasia"

## Dependências

    sudo apt install make hugo

## Testando

    make serve

## Compilando

    make compile
